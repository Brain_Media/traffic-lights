import Vue from 'vue'
import { mount } from 'avoriaz'
import Lights from '@/components/Lights'

describe('Lights.vue', () => {
    it('Builds lights array', () => {
        const LightsComponent = mount(Lights)
        expect(LightsComponent.data().lights.length).to.equal(2)
    })

    it('NS lights should be green at the start', () => {
        const LightsComponent = mount(Lights)
        expect(LightsComponent.data().lights[0].status).to.equal(1)
    })

    it('E,W lights should be red at the start', () => {
        const LightsComponent = mount(Lights)
        expect(LightsComponent.data().lights[1].status).to.equal(0)
    })

    it('Clicking on `Run` should set running to true', () => {
        const LightsComponent = mount(Lights)
        const runButton = LightsComponent.find('button')[0]
        runButton.trigger('click')
        expect(LightsComponent.data().lines.length).to.equal(2)
    })

    it('Clicking on `Stop` should set running to false', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.run()
        expect(vm.isRunning).to.equal(true)
        vm.stop()
        expect(vm.isRunning).to.equal(false)
    })

    it('switch NS to yellow', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.startTransition()
        expect(vm.lights[0].colours[1].status).to.equal(1)
    })

    it('switch NS to red', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.startTransition()
        vm.finishTransition()
        expect(vm.lights[0].colours[2].status).to.equal(1)
        expect(vm.lights[0].colours[0].status).to.equal(0)
    })

    it('switch EW to green', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.startTransition()
        vm.finishTransition()
        expect(vm.lights[1].colours[2].status).to.equal(0)
        expect(vm.lights[1].colours[0].status).to.equal(1)
    })

    it('should switch NS to yellow automatically after 9 seconds', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.cycleDuration = 10
        vm.transitionDuration = 1
        vm.totalDuration = 9
        
        vm.checkDuration()
        expect(vm.lights[0].colours[1].status).to.equal(1)
    })

    it('should switch NS to red and EW to green automatically after 10 seconds', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.cycleDuration = 10
        vm.transitionDuration = 1
        vm.totalDuration = 10

        vm.checkDuration()
        expect(vm.lights[0].colours[2].status).to.equal(1)
        expect(vm.lights[1].colours[0].status).to.equal(1)
    })

    it('should revert colours automatically after 20 seconds', () => {
        const Constructor = Vue.extend(Lights)
        const vm = new Constructor().$mount()
        vm.cycleDuration = 10
        vm.transitionDuration = 1
        vm.totalDuration = 20

        vm.checkDuration()
        expect(vm.lights[0].colours[0].status).to.equal(1)
        expect(vm.lights[1].colours[2].status).to.equal(1)
    })
})
