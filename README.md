# Install dependencies
npm install

# Run app at localhost:8080
npm run dev

# Run unit tests
npm run unit
